import pandas as pd

# main - using pandas to get close date 21 days later
def main(): 
  data = [[130.32, '2018-12-03'],[147.24, '2018-12-04'],[150.28, '2018-12-06'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[147.24, '2018-12-04'],[150.28, '2018-12-06'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[147.24, '2018-12-04'],[150.28, '2018-12-06'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[147.24, '2018-12-04'],[150.28, '2018-12-06'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03'],[161.36, '2018-12-07'],[160.44,'2018-12-10'],[130.32, '2018-12-03']]

  vxx_history = pd.DataFrame(data, columns = ['VXX_Close', 'date'])

  x = 0
  while x < len(vxx_history) - 21:
    vxx_history.loc[x, 'Close21DaysLater'] = vxx_history.iloc[x+21]['VXX_Close']
    x += 1

  print(vxx_history)
if __name__ == "__main__":
  main()
